<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function episodes()
    {

        return $this->hasMany(Episode::class);

    }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }
}
