<?php

namespace App\Http\Controllers;

use App\Course;
use App\Episode;
use Illuminate\Http\Request;
use function MongoDB\BSON\toJSON;
use Vimeo\Vimeo;

class EpisodeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd(\request()->all());
        return view();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Episode $episode)
    {
        $this->validate($request, [
            'name'      => 'required',
            'video'     => 'required',
            'course_id' => 'required',
        ]);
        $path = $request->file('video')->store('videos', 's3');

//        if ($request->hasFile('video')) {
//            $path = $request->file('video')->storePublicly('videos', 'public');
//        }

        $episode->video = $path;
        $episode->name = $request->name;
        $episode->course_id = $request->course_id;
        $episode->save();

        return response($episode);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Episode  $episode
     * @return \Illuminate\Http\Response
     */
    public function show(Course $episode)
    {
        $course = $episode;
        return view('episodes.create', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Episode  $episode
     * @return \Illuminate\Http\Response
     */
    public function edit(Episode $episode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Episode  $episode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Episode $episode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Episode  $episode
     * @return \Illuminate\Http\Response
     */
    public function destroy(Episode $episode)
    {
        //
    }
}
