@extends('layouts.app')

@section('content')

    <div class="col-12">
        <div class="hk-pg-header">
            <h4 class="hk-pg-title">
                <span class="pg-title-icon"><span class="feather-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                            stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                            class="feather feather-type"><polyline points="4 7 4 4 20 4 20 7"></polyline><line x1="9"
                                                                                                               y1="20"
                                                                                                               x2="15"
                                                                                                               y2="20"></line><line
                                x1="12" y1="4" x2="12" y2="20"></line></svg></span></span> Add Course
            </h4>
        </div>
        <div class="col-xl-12">
            <addcourse></addcourse>
        </div>
    </div>
@endsection
