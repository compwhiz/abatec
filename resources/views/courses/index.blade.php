@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="hk-row pt-4">
                @foreach(\App\Course::all() as $course)
                    <div class="col-md-4">
                        {{-- src="https://superhumanacademy.com/wp-content/uploads/2015/01/become-a-superlearner.jpg"--}}

                        <div class="card mb-3">
                            <img style="display:flex;background-size:cover; overflow: hidden;"
                                src="https://pichaclick1.s3.us-east-2.amazonaws.com/{{ $course->filepath  }}"
                                class="card-img-top img-fluid" >
                            <div class="card-body">
                                <h5 class="card-title">
                                    <a href="{{ route('courses.show',$course->id) }}">{{ $course->course }}</a></h5>
                                <p class="card-text">{{ strip_tags($course->description) }}</p>
                                <p class="card-text">
{{--                                    <small class="text-muted">4 Hours</small>--}}
{{--                                    <button class="float-right btn btn-outline-info btn-sm">Purchase--}}
{{--                                        <i class="fa fa-money"></i>--}}
{{--                                    </button>--}}
{{--                                    @if(request()->user()->)--}}
                                    <a class="float-right btn btn-success btn-sm"
                                    href="{{ route('courses.show',$course->id) }}"
                                    > Learn <i class="fa fa-eye"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
