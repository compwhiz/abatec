@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="hk-row">

                <div class="col-md-12 pa-0">
                    <div class="profile-cover-wrap overlay-wrap">
                        <div class="profile-cover-img" style="background-image:url('dist/img/trans-bg.jpg')"></div>
                        <div class="bg-overlay bg-trans-dark-60"></div>
                        <div class="container-fluid profile-cover-content py-50">
                            <div class="hk-row">
                                <div class="col-lg-12">
                                    <div class="media align-items-center">
                                        <div class="media-img-wrap  d-flex">
                                        </div>
                                        <div class="media-body">
                                            <div class="text-white text-capitalize display-6 mb-5 font-weight-400">
                                                {{ $course->course }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="hk-row">
                        <div class="col-lg-12">
                            <div style="position:relative;">
                                <video
                                    id="my-player"
                                    class="video-js"
                                    controls
                                    preload="auto"
                                    poster="https://pichaclick1.s3.us-east-2.amazonaws.com/{{ $course->filepath  }}"
{{--                                    src="{{ "https://pichaclick1.s3.us-east-2.amazonaws.com/".$course->episodes->first()->video }}"--}}
                                >
                                    <source src="{{ "https://pichaclick1.s3.us-east-2.amazonaws.com/"
                                    .$course->episodes->first()->video }}"></source>
{{--                                    <source src="//vjs.zencdn.net/v/oceans.webm" type="video/webm"></source>--}}
{{--                                    <source src="//vjs.zencdn.net/v/oceans.ogv" type="video/ogg"></source>--}}
                                    <p class="vjs-no-js">
                                        To view this video please enable JavaScript, and consider upgrading to a
                                        web browser that
                                        <a href="https://videojs.com/html5-video-support/" target="_blank">
                                            supports HTML5 video
                                        </a>
                                    </p>
                                </video>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mt-4">
        <div class="row">
            <div class="col-md-8">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Description</h5>
                            <p class="card-text">{{ $course->course }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Requirements</h5>
                            {{ Strip_tags( $course->requirements) }}
                            {{--                            <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>--}}
                            <p class="card-text">Access to a computer with an internet connection.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                @foreach($course->episodes as $episode)
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action">
                            <p>Episode {{ $loop->iteration }}
                                <span class="float-right"><i class="fa fa-clock-o"></i>RUN TIME11:42</span>
                            </p>
                            <h6>{{$episode->name}}</h6>
                        </a>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection
