@extends('layouts.app')

@section('content')

    <div class="col-12">
        <div class="hk-pg-header">
            <h4 class="hk-pg-title">
                <span class="pg-title-icon"></span> Add Episodes to &nbsp;<a href="{{ route('courses.show',$course->id)
                }}">{{
                $course->course  }}</a>
            </h4>
        </div>
        <div class="col-xl-12">
           <div class="row">
               <div class="col-12">
                   <addepisode :courses="{{ $course }}"></addepisode>
               </div>
           </div>
        </div>
    </div>
@endsection
