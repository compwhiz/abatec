@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="hk-row">
                <div class="col-lg-3 col-sm-6">
                    <div class="card card-sm">
                        <div class="card-body">
                                    <span class="d-block font-11 font-weight-500 text-dark text-uppercase
                                    mb-10">My Courses</span>
                            <div class="d-flex align-items-center justify-content-between position-relative">
                                {{--                                        <div>--}}
                                {{--                                            <span class="d-block display-5 font-weight-400 text-dark">1</span>--}}
                                {{--                                        </div>--}}
                                <div>
                                    <a href="/courses" class="btn btn-sm btn-outline-info">Choose Course
                                        <i class="fa fa-plus"></i></a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="card card-sm">
                        <div class="card-body">
                                    <span
                                        class="d-block font-11 font-weight-500 text-dark text-uppercase mb-10">My Mentor
                                    </span>
                            <div class="d-flex align-items-center justify-content-between position-relative">
                                <div>
                                    {{--												<span class="d-block">--}}
                                    {{--													<span--}}
                                    {{--                                                        class="display-5 font-weight-400 text-dark">Kennedy --}}
                                    {{--                                                        Mutisya</span>--}}
                                    {{--												</span>--}}
                                    <div>
                                        <a href="/courses" class="btn btn-sm btn-outline-info">Get a Mentor
                                            <i class="fa fa-user"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="hk-row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body pa-0">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table class="table table-hover mb-0">
                                        <thead>
                                        <tr>
                                            <th>Course</th>
                                            <th>Date Course Started</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach(request()->user()->course as $course)
                                            <tr>
                                                <td scope="row">{{ $course->course }}</td>
                                                <td>{{ $course->created_at->toFormattedDateString() }}</td>
                                                <td>
                                                    <a href="{{ route('courses.show',$course->id) }}"> Continue</a>
                                                </td>
                                                <td>
                                                    <a class="btn-outline-danger btn-sm" href="{{ url('/stopcourse')
                                                    ."/".$course->id
                                                     }}"> Stop
                                                        taking
                                                        this course</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
