<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang=en> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang=en> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang=en> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang=en>

<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge"/>
    <meta name=viewport content="width=device-width, initial-scale=1.0"/>
    <title>Abetec Business Solutions</title>
    <link rel="shortcut icon" href=images/favicon.ico type=image/x-icon>
    <link rel=icon href=images/favicon.ico type=image/x-icon>
    <link rel=apple-touch-icon sizes=152x152 href=images/apple-touch-icon-152x152.png>
    <link rel=apple-touch-icon sizes=120x120 href=images/apple-touch-icon-120x120.png>
    <link rel=apple-touch-icon sizes=76x76 href=images/apple-touch-icon-76x76.png>
    <link rel=apple-touch-icon href=images/apple-touch-icon.png>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel=stylesheet>
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC:300,400,500,700,900" rel=stylesheet>
    <link href=css/bootstrap.min.css rel=stylesheet>
    <link href=../../../../use.fontawesome.com/releases/v5.7.2/css/all.css
          integrity=sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr rel=stylesheet
          crossorigin=anonymous>
    <link href=css/flaticon.css rel=stylesheet>
    <link href=css/magnific-popup.css rel=stylesheet>
    <link href=css/owl.carousel.min.css rel=stylesheet>
    <link href=css/owl.theme.default.min.css rel=stylesheet>
    <link href=css/animate.css rel=stylesheet>
    <link href=css/purple-theme.css rel=stylesheet>
    <link href=css/aqua-theme.css rel="alternate stylesheet" title=aqua-theme>
    <link href=css/blue-theme.css rel="alternate stylesheet" title=blue-theme>
    <link href=css/bluegrey-theme.css rel="alternate stylesheet" title=bluegrey-theme>
    <link href=css/darkblue-theme.css rel="alternate stylesheet" title=darkblue-theme>
    <link href=css/green-theme.css rel="alternate stylesheet" title=green-theme>
    <link href=css/darkgreen-theme.css rel="alternate stylesheet" title=darkgreen-theme>
    <link href=css/deeporange-theme.css rel="alternate stylesheet" title=deeporange-theme>
    <link href=css/red-theme.css rel="alternate stylesheet" title=red-theme>
    <link href=css/tomato-theme.css rel="alternate stylesheet" title=tomato-theme>
    <link href=css/responsive.css rel=stylesheet>
</head>
<body>
<div id=loader-wrapper>
    <div id=loader>
<span class=cssload-loader>
<span class=cssload-loader-inner></span>
</span>
    </div>
</div>
<div id=stlChanger>
    <div class="blockChanger bgChanger">
        <a href=# class="chBut icon-xs"><span class=flaticon-050-settings></span></a>
        <div class=chBody>
            <div class="stBlock text-center" style="margin:30px 20px 20px 16px">
                <p>Color Scheme</p>
            </div>
            <div class="stBlock1 text-center">
                <a class="btn btn-primary-color black-hover mt-15" href="javascript:chooseStyle('none',60)">Reset
                    color</a>
            </div>

        </div>
    </div>
</div>
<div id=page class=page>
    <header id=header class=header>
        <nav class="navbar fixed-top navbar-expand-md navbar-dark bg-tra hover-menu">
            <div class=container>
                <a href=#hero-15 class="navbar-brand logo-black">
                    <img src="{{ asset('0.png') }}" width=120 height="90" alt=header-logo></a>
                <a href=#hero-15 class="navbar-brand logo-white">
                    <img src="{{ asset('0.png') }}" width=120  alt=header-logo></a>
                <button class=navbar-toggler type=button data-toggle=collapse data-target=#navbarSupportedContent
                        aria-controls=navbarSupportedContent aria-expanded=false aria-label="Toggle navigation">
                    <span class=navbar-bar-icon><i class="fas fa-bars"></i></span>
                </button>
                <div id=navbarSupportedContent class="collapse navbar-collapse">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href=# id=navbarDropdownMenuLink data-toggle=dropdown
                               aria-haspopup=true aria-expanded=false>
                                About
                            </a>
                            <ul class=dropdown-menu aria-labelledby=navbarDropdownMenuLink>
                                <li><a class=dropdown-item href=#services-8>Our Services</a></li>
                                <li><a class=dropdown-item href=#content-7>Why Abetec</a></li>
                                <li><a class=dropdown-item href=#content-8>What We Offer</a></li>
                                <li><a class=dropdown-item href=#statistic-1>Best Solutions</a></li>
                                <li><a class=dropdown-item href=#reviews-2>Testimonials</a></li>
                            </ul>
                        </li>
                        <li class="nav-item nl-simple"><a class=nav-link href=#pricing-1>Pricing</a></li>
                        <li class="nav-item nl-simple"><a class=nav-link href=#faqs-2>FAQs</a></li>
{{--                        <li class="nav-item dropdown">--}}
{{--                            <a class="nav-link dropdown-toggle" href=# id=navbarDropdownMenuLink data-toggle=dropdown--}}
{{--                               aria-haspopup=true aria-expanded=false>--}}
{{--                                Pages--}}
{{--                            </a>--}}
{{--                            <ul class=dropdown-menu aria-labelledby=navbarDropdownMenuLink>--}}
{{--                                <li><a class=dropdown-item href=blog-listing.html>Blog Listing</a></li>--}}
{{--                                <li><a class=dropdown-item href=single-post.html>Single Blog Post</a></li>--}}
{{--                                <li><a class=dropdown-item href=about.html>About Abetec</a></li>--}}
{{--                                <li><a class=dropdown-item href=pricing.html>Pricing Plans</a></li>--}}
{{--                                <li><a class=dropdown-item href=contacts.html>Contacts Us</a></li>--}}
{{--                                <li><a class=dropdown-item href=clients.html>Our Clients</a></li>--}}
{{--                                <li><a class=dropdown-item href=faqs.html>FAQs</a></li>--}}
{{--                                <li><a class=dropdown-item href=term.html>Terms & Privacy</a></li>--}}
{{--                                <li><a class=dropdown-item href=404.html>404 Page</a></li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
                        <li class="nav-item nl-simple"><a class=nav-link href=#blog-1>Blog</a></li>
                    </ul>
                    <span class="navbar-text white-color">
                        <a href="/login" class="btn btn-tra-white yellow-hover">Login</a>
                    </span>
                </div>
            </div>
        </nav>
    </header>
    <section id=hero-15 class="bg-scroll hero-section division">
        <div class=container>
            <div class=row>
                <div class="col-md-7 col-lg-6 col-xl-5">
                    <div class="hero-txt white-color">
                        <h2>We support the youth and women through: -</h2>
                        <ul class=content-list>
                            <li><p class=p-md>Our Online Mentorship Program</p></li>
                            <li><p class=p-md>Training</p></li>
                            <li><p class=p-md>Transformation and Empowerment</p></li>
                        </ul>
                        <div class="hero-btns mt-30">
                            <a href="/register" class="btn btn-yellow tra-hover">Become a Member</a>
                            <a href="/donate" class="video-popup2 btn btn-tra">
                                Donate <i class="far fa-play-circle"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-lg-6 col-xl-6 offset-xl-1">
                    <div class="hero-15-img text-center">
                        <img class=img-fluid src={{ asset('coursesone.png') }} alt=hero-image height="400">
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-fixed hero-15-wave"></div>
    </section>
    <section id=services-8 class="pt-80 pb-50 services-section division">
        <div class=container>
            <div class=row>
                <div class="col-lg-10 offset-lg-1 section-title">
                    <p class=p-md>Abetec Business Solutions Limited is a Social Enterprise and a business incubator
                        whose main aim is to support Youths and Women as they transit from colleges to the job
                        market.</p>
                </div>
            </div>
            <div class=row>
                <div class="col-lg-10 offset-lg-1">
                    <div class=row>
                        <div class=col-sm-6>
                            <div class="sbox-4 wow fadeInUp" data-wow-delay=0.4s>
                                <img class=img-70 src=images/icons/experiment.png alt=service-icon/>
                                <div class=sbox-4-txt>
                                    <h5 class="h5-sm steelblue-color">Training</h5>
                                    <p class=grey-color>Words on training</p>
                                </div>
                            </div>
                        </div>
                        <div class=col-sm-6>
                            <div class="sbox-4 wow fadeInUp" data-wow-delay=0.6s>
                                <img class=img-70 src=images/icons/idea-2.png alt=service-icon/>
                                <div class=sbox-4-txt>
                                    <h5 class="h5-sm steelblue-color">Mentorship Program</h5>
                                    <p class=grey-color>Words on mentorship</p>
                                </div>
                            </div>
                        </div>
                        <div class=col-sm-6>
                            <div class="sbox-4 wow fadeInUp" data-wow-delay=0.4s>
                                <img class=img-70 src=images/icons/presentation.png alt=service-icon/>
                                <div class=sbox-4-txt>
                                    <h5 class="h5-sm steelblue-color">MarkertPlace </h5>
                                    <p class=grey-color>Words on markert place</p>
                                </div>
                            </div>
                        </div>
                        <div class=col-sm-6>
                            <div class="sbox-4 wow fadeInUp" data-wow-delay=0.6s>
                                <img class=img-70 src=images/icons/wallet-1.png alt=service-icon/>
                                <div class=sbox-4-txt>
                                    <h5 class="h5-sm steelblue-color">Work Stations</h5>
                                    <p class=grey-color>Words on workstations</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id=content-2 class="bg-lightgrey wide-60 content-section division">
        <div class=container>
            <div class="row d-flex align-items-center">
                <div class=col-md-6>
                    <div class="img-block wow fadeInUp" data-wow-delay=0.6s>
                        <img class=img-fluid src=images/image-05.png alt=content-image>
                    </div>
                </div>
                <div class=col-md-6>
                    <div class="txt-block pc-30 wow fadeInUp" data-wow-delay=0.4s>
                        <span class="section-id primary-color">Digital Literacy</span>
                        <h3 class="h3-md steelblue-color">Digital Literacy</h3>
                        <p>We partner with key organisations to empower Youths and women on Digital literacy</p>
                        <h5 class="h5-md steelblue-color">Powering Schools in Mombasa</h5>
                        <div class="box-list m-top-15">
                            <div class=box-list-icon><i class="fas fa-genderless"></i></div>
                            <p>We partner with key organisations to empower Youths and women on Digital literacy</p>
                        </div>
                        <div class=box-list>
                            <div class=box-list-icon><i class="fas fa-genderless"></i></div>
                            <p>We partner with key organisations to empower Youths and women on Digital literacy</p>
                        </div>
                        <div class=box-list>
                            <div class=box-list-icon><i class="fas fa-genderless"></i></div>
                            <p>We partner with key organisations to empower Youths and women on Digital literacy</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id=content-7 class="wide-60 content-section division">
        <div class=container>
            <div class="row d-flex align-items-center">
                <div class="col-md-6 col-xl-6">
                    <div class="txt-block pc-30 wow fadeInUp" data-wow-delay=0.4s>
                        <span class="section-id primary-color">Initiatives</span>
                        <h3 class="h3-md steelblue-color">Initiatives that encourage innovative growth among the
                            youth and Women.</h3>
                        <div class=cbox-1>
                            <i class="fas fa-circle"></i>
                            <div class="cbox-1-txt grey-color">
                                <p>Initiatives that encourage innovative growth among the
                                    youth and Women.</p>
                            </div>
                        </div>
                        <div class=cbox-1>
                            <i class="fas fa-circle"></i>
                            <div class="cbox-1-txt grey-color">
                                <p>Initiatives that encourage innovative growth among the
                                    youth and Women.</p>
                            </div>
                        </div>
                        <div class="tools-list mt-15">
                            <h5 class=h5-xs>Technologies We Use:</h5>
                            <i class="fa fa-html5"></i>
                            <i class="fa fa-css3-alt"></i>
                            <i class="fa fa-js-square"></i>
                            <i class="fa fa-php"></i>
                            <i class="fa fa-mailchimp"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-5 offset-xl-1">
                    <div class="content-7-img wow fadeInRight" data-wow-delay=0.6s>
                        <img class=img-fluid src=images/image-08.png alt=content-image>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id=pricing-1 class="bg-lightgrey wide-60 pricing-section">
        <div class=container>
            <div class=row>
                <div class="col-lg-10 offset-lg-1 section-title">
                    <h3 class="h3-md steelblue-color">Simple Pricing, Instant Sign Up</h3>
                    <p class=p-md>Choose your plan</p>
                </div>
            </div>
            <div class="row d-flex align-items-center pricing-row primary-theme">
                <div class=col-md-4>
                    <div class="pricing-table wow fadeInUp" data-wow-delay=0.4s>
                        <div class="pricing-plan steelblue-color">
                            <h5 class=h5-xs>Basic</h5>
                            <sup>KES</sup>
                            <span class=price>500</span>
                            <sup class=pricing-coins>99</sup>
                            <p class=p-md>monthly</p>
                        </div>
                        <ul class="features steelblue-color">
                            <li><i class="fas fa-stop-circle"></i> Group WorkStations</li>
                            <li><i class="fas fa-stop-circle"></i> For Group Members</li>
                            <li class=disabled-option><i class="fas fa-stop-circle"></i> Desk</li>
                            <li class=disabled-option><i class="fas fa-stop-circle"></i> Internet</li>
                            <li class=disabled-option><i class="fas fa-stop-circle"></i> Free Lunch Pass</li>
                        </ul>
                        <a href=# class="btn btn-tra-grey black-hover">Select Plan</a>
                    </div>
                </div>
                <div class=col-md-4>
                    <div class="pricing-table highlight wow fadeInUp" data-wow-delay=0.7s>
                        <div class="pricing-plan steelblue-color">
                            <h5 class=h5-xs>Standard</h5>
                            <sup>KES</sup>
                            <span class=price>5000</span>
                            <sup class=pricing-coins>99</sup>
                            <p class=p-md>monthly</p>
                        </div>
                        <ul class="features steelblue-color">
                            <li><i class="fas fa-stop-circle"></i> Individual Access</li>
                            <li><i class="fas fa-stop-circle"></i> For personal study and work</li>
                            <li><i class="fas fa-stop-circle"></i> Desk</li>
                            <li class=disabled-option><i class="fas fa-stop-circle"></i> Internet</li>
                            <li class=disabled-option><i class="fas fa-stop-circle"></i> File Cabinet</li>
                        </ul>
                        <a href=# class="btn btn-primary-color black-hover">Select Plan</a>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <div id=statistic-1 class="blue-waves wide-60 statistic-section division">
        <div class="container white-color">
            <div class=row>
                <div class="col-lg-10 offset-lg-1 section-title">
                    <h3 class=h3-md>More Faster, More Powerful </h3>
                    <p class=p-md>We are impacting lifes of the youth and womwnt</p>
                </div>
            </div>
            <div class=row>
                <div class="col-md-10 col-lg-8 offset-md-1 offset-lg-2">
                    <div class=row>
                        <div class=col-sm-4>
                            <div class="statistic-block wow fadeInUp" data-wow-delay=0.4s>
                                <h5 class=statistic-number><span class=count-element>38</span>%</h5>
                                <p class=txt-400>Digital Literacy Penetration</p>
                            </div>
                        </div>
                        <div class=col-sm-4>
                            <div class="statistic-block wow fadeInUp" data-wow-delay=0.6s>
                                <h5 class=statistic-number><span class=count-element>47</span>%</h5>
                                <p class=txt-400>Coverage in Mombasa</p>
                            </div>
                        </div>
                        <div class=col-sm-4>
                            <div class="statistic-block wow fadeInUp" data-wow-delay=0.8s>
                                <h5 class=statistic-number><span class=count-element>90</span>%</h5>
                                <p class=txt-400>Time to deliver</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id=brands-1 class="brands-section division">
        <div class=container>
            <div class=row>
                <div class="col-md-12 text-center mb-10">
                    <p>We partner with companies of all sizes, all around the world</p>
                </div>
            </div>
            <div class="row brands-holder">
                <div class="col-sm-4 col-lg-2">
                    <div class=brand-logo>
                        <img class=img-fluid src=images/brand-1.png alt=brand-logo/>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-2">
                    <div class=brand-logo>
                        <img class=img-fluid src=images/brand-2.png alt=brand-logo/>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-2">
                    <div class=brand-logo>
                        <img class=img-fluid src=images/brand-3.png alt=brand-logo/>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-2">
                    <div class=brand-logo>
                        <img class=img-fluid src=images/brand-4.png alt=brand-logo/>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-2">
                    <div class=brand-logo>
                        <img class=img-fluid src=images/brand-5.png alt=brand-logo>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-2">
                    <div class=brand-logo>
                        <img class=img-fluid src=images/brand-6.png alt=brand-logo/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section id=reviews-2 class="bg-lightgrey wide-80 reviews-section division">
        <div class=container>
            <div class=row>
                <div class="col-lg-10 offset-lg-1 section-title">
                    <h3 class="h3-md steelblue-color">What Our Customers Are Saying</h3>
                    <p class=p-md>What our partners and customers are saying</p>
                </div>
            </div>
            <div class=row>
                <div class="col-md-12 reviews-grid">
                    <div class="masonry-wrap grid-loaded">
                        <div class=review-2>
                            <div class=review-txt>
                                <div class="author-data clearfix">
                                    <div class=testimonial-avatar>
                                        <img src=images/review-author-1.jpg alt=testimonial-avatar>
                                    </div>
                                    <div class=review-author>
                                        <h5 class=h5-xs>pebz13</h5>
                                        <div class=rating>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star-half"></i>
                                        </div>
                                    </div>
                                </div>
                                <p>Etiam sapien sem at sagittis congue an augue massa varius egestas a suscipit
                                    magna tempus and aliquet porta
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id=cta-2 class="bg-image wide-100 cta-section division">
        <div class="container white-color">
            <div class=row>
                <div class="col-lg-10 offset-lg-1">
                    <div class="cta-txt text-center">
                        <h3 class=h3-xs>Create better experiences with Abetec</h3>
                        <p class=p-md>Get more with Abetec Online Programs</p>
                        <div class=btns-group>
                            <a href=# class="btn btn-primary-color tra-hover mr-15">Request Free Trial</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id=blog-1 class="wide-60 blog-section division">
        <div class=container>
            <div class=row>
                <div class="col-lg-10 offset-lg-1 section-title">
                    <h3 class=h3-lg>Our Stories & Latest News</h3>
                    <p class=p-md>Our latest stories and news</p>
                </div>
            </div>
            <div class="row primary-theme">
                <div class="col-md-6 col-lg-4">
                    <div class="blog-post wow fadeInUp" data-wow-delay=0.4s>
                        <div class=blog-post-img>
                            <div class=post-category><p>News</p></div>
                            <img class=img-fluid src=images/blog/post-1-img.jpg alt=blog-post-image/>
                            <div class=blog-post-avatar>
                                <img src=images/post-author-1.jpg alt=author-avatar>
                            </div>
                        </div>
                        <div class=blog-post-txt>
                            <p class=post-meta>Posted by <span>Betty K</span> on August 7, 2019</p>
                            <a href=single-post.html>Some new story</a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <section id=contacts-1 class="bottom-clouds wide-100 contacts-section division">
        <div class=container>
            <div class=row>
                <div class="col-lg-10 offset-lg-1 section-title">
                    <h3 class="h3-md steelblue-color">Looking For Support? Get In Touch</h3>
                    <p class=p-md>We are available 24/7</p>
                </div>
            </div>
            <div class=row>
                <div class="col-md-10 col-xl-8 offset-md-1 offset-xl-2">
                    <div class=form-holder>
                        <form name=contactform class="row contact-form">
                            <div id=input-name class=col-lg-6>
                                <input type=text name=name class="form-control name" placeholder="Your Name*">
                            </div>
                            <div id=input-email class=col-lg-6>
                                <input type=text name=email class="form-control email" placeholder="Email Address*">
                            </div>
                            <div id=input-subject class="col-md-12 input-subject">
                                <select id=inlineFormCustomSelect1 name=Subject class="custom-select subject">
                                    <option>This question is about...</option>
                                    <option>Registering/Authorising</option>
                                    <option>Using Abetec</option>
                                    <option>Troubleshooting</option>
                                    <option>Backup/Restore</option>
                                    <option>Other</option>
                                </select>
                            </div>
                            <div id=input-message class="col-lg-12 input-message">
                                <textarea class="form-control message" name=message rows=6
                                          placeholder="Your Message ..."></textarea>
                            </div>
                            <div class="col-lg-12 mt-15 form-btn">
                                <button type=submit class="btn btn-primary-color black-hover submit">Send Your Message
                                </button>
                            </div>
                            <div class="col-lg-12 contact-form-msg text-center">
                                <div class=sending-msg><span class=loading></span></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer id=footer-2 class="wide-40 footer division">
        <div class=container>
            <div class=row>
                <div class="col-md-10 col-lg-4">
                    <div class="footer-info mb-40">
                        <img src="{{ asset('0.png') }}" width=175 height=100 alt=footer-logo>
                        <p class=foo-email>E: betty@abetech.co.ke</p>
                        <p>P: +254 722 966 444</p>
                        <div class="footer-socials-links mt-20">
                            <ul class="foo-socials text-center clearfix">
                                <li><a href=# class=ico-facebook><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href=# class=ico-twitter><i class="fab fa-twitter"></i></a></li>
                                <li><a href=# class=ico-google-plus><i class="fab fa-google-plus-g"></i></a></li>
                                <li><a href=# class=ico-tumblr><i class="fab fa-tumblr"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-2 offset-lg-1">
                    <div class="footer-links mb-40">
                        <h5 class=h5-sm>Products</h5>
                        <ul class=clearfix>
                            <li><a href=#>How It Works?</a></li>
                            <li><a href=#>Integrations</a></li>
                            <li><a href=#>Product Updates</a></li>
                            <li><a href=#>Our Pricing</a></li>
                            <li><a href=#>FAQs</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-lg-2">
                    <div class="footer-links mb-40">
                        <h5 class=h5-sm>Company</h5>
                        <ul class=clearfix>
                            <li><a href=#>About Us</a></li>
                            <li><a href=#>Careers</a></li>
                            <li><a href=#>Press & Media</a></li>
                            <li><a href=#>Our Blog</a></li>
                            <li><a href=#>Advertising</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="footer-form mb-20">
                        <h5 class=h5-sm>Subscribe Us:</h5>
                        <p class="p-sm m-bottom-20">Stay up to date with our latest news, updates and our new
                            products</p>
                        <form class=newsletter-form>
                            <div class=input-group>
                                <input type=email class=form-control placeholder="Email Address" required id=s-email>
                                <span class=input-group-btn>
                                    <button type=submit class=btn>
                                        <i class="far fa-envelope"></i>
                                    </button>
                                </span>
                            </div>
                            <label for=s-email class=form-notification></label>
                        </form>
                    </div>
                </div>
            </div>
            <div class=bottom-footer>
                <div class=row>
                    <div class=col-md-12>
                        <p class=footer-copyright>&copy; 2019 <span>Abetec</span>. All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<script data-cfasync="false" src="../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
<script src=js/jquery-3.3.1.min.js></script>
<script src=js/bootstrap.min.js></script>
<script src=js/modernizr.custom.js></script>
<script src=js/jquery.easing.js></script>
<script src=js/jquery.appear.js></script>
<script src=js/jquery.stellar.min.js></script>
<script src=js/jquery.scrollto.js></script>
<script src=js/owl.carousel.min.js></script>
<script src=js/jquery.magnific-popup.min.js></script>
<script src=js/imagesloaded.pkgd.min.js></script>
<script src=js/isotope.pkgd.min.js></script>
<script src=js/hero-form.js></script>
<script src=js/register-form.js></script>
<script src=js/contact-form.js></script>
<script src=js/quick-form.js></script>
<script src=js/comment-form.js></script>
<script src=js/jquery.validate.min.js></script>
<script src=js/jquery.ajaxchimp.min.js></script>
<script src=js/wow.js></script>
<script src=js/custom.js></script>
<script>new WOW().init();</script>
<!-- [if lt IE 9]>
<script src=js/html5shiv.js type=text/javascript></script>
<script src=js/respond.min.js type=text/javascript></script>
<![endif] -->
<script src=js/changer.js></script>
<script defer src=js/styleswitch.js></script>
</body>

</html>
