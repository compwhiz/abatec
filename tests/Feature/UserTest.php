<?php

namespace Tests\Feature;

use App\Role;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @test
     */
    public function A_User_can_Be_made_into_an_admin()
    {
        $user = factory(User::class)->create();

        $role = factory(Role::class)->create(['role'=>'admin']);

        $user->makeAdmin();

        $this->assertSame(1,$user->role->id);
    }
}
