<?php
//dd(public_path());
Route::get('/', function () {
    return view('welcome');
});

Route::resource('courses','CourseController');

Route::resource('episodes','EpisodeController');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('cpanel', "CPANEL@login");
Route::get('stopcourse/{course}','CourseController@destroy');
Auth::routes();

// This is listening to the bitbucket webhook to update cpanel in realtime.
Route::post('cpanel', "CPANEL@cpanel");

Route::get('/install', function () {

    Artisan::call('migrate');

    return Artisan::output();
});

Route::post('none', function () {
    return "all good";
});
Route::get('none', function () {
    return "all good";
});
Route::get('/link', function () {

    Artisan::call('storage:link');

    return Artisan::output();
});

Route::get('clear', function () {
    $allCourses = \App\Course::all();

    $allCourses->each->delete();

});
